global.fetch = require("node-fetch");
fetch('https://jsonplaceholder.typicode.com/posts')
  .then(response => response.json())
  .then(posts => {
    let obj={}
    for (let p of posts) {
      obj[p.id]=p;
    }
    fetch('https://jsonplaceholder.typicode.com/comments')
      .then(response => response.json())
      .then(comments => {
        for (let cm of comments) {
          if(!obj[cm.postId].comments)
          obj[cm.postId].comments=[cm]
          else {
            obj[cm.postId].comments.push(cm)
          }
        }
        console.log(Object.values(obj));
      })
  })
