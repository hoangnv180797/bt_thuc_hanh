//đếm số từ cần tìm

const fs = require("fs");
const data = fs.readFileSync('input.txt');

// handleStr(data, "nhà")
//
// getMail1(data)
//
// getPhone1(data)
//
// getDate1(data)

function handleStr(str,find=""){
    let cout=0,first=-1,last=-1,pos=0
    while (true) {
        pos=str.indexOf(find,pos);
        if(pos!=-1){
          cout++;
          last=pos;
          pos++;
          if(first==-1){
            first=pos;
          }
        }else {
          break;
        }
    }
    console.log(cout,first,last)
}

function getMail1(source){
  console.log( String(source).match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi));
}

function getPhone1(source){
  source=String(source)
  console.log(source.match(/[0-9]{10}/g));
}

// xử lý datetime

function getDate1(source){
  console.log( String(source).match(/[0-9]{1,2}([./-])[0-9]{1,2}([./-])[0-9]{4}/g));
}



// outputTime("ngày hôm nay sau")

function outputTime(str="") {
  if(str.indexOf("hôm nay") != -1){
    const today = new Date();
    const date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    console.log(date);
  }
  if(str.indexOf("tháng sau") != -1){
    const today = new Date();
    const date = today.getFullYear()+'-'+(today.getMonth()+2)+'-'+today.getDate();
    console.log(date);
  }
}

//xử lý Json



// mảng
const hs = [
    {
        "name": "cat",
        "age": "18",
        "sex": "nam",
        "dtb": 2
    },
    {
      "name": "beer",
      "age": "19",
      "sex": "nữ",
      "dtb": 2.6
    },
    {
      "name": "lion",
      "age": "18",
      "sex": "nam",
      "dtb": 3.4
    },
    {
      "name": "tiger",
      "age": "18",
      "sex": "nam",
      "dtb": 2.8
    }
]

// count_hs_sex("nữ")

function count_hs_sex(find="") {
  if(find === "nữ"){
    let hs_nu = hs.filter((hs) => {
        return hs.sex === "nữ"
    })
    console.log(hs_nu.length);
  }
  else if (find === "nam") {
    let hs_nam = hs.filter((hs) => {
        return hs.sex === "nam"
    })
    console.log(hs_nam.length);
  }
  else {
    console.log("Nhap sai");
  }
}

// hien thi hoc sinh theo diem

// hienthitheophanloai("A")

function hienthitheophanloai(find="") {
  if (find === "A") {
    let hs_a = hs.filter((hs) => {
        if(hs.dtb >= 2 && hs.dtb <2.5){
          return hs;
        }
    })
    for (let i = 0; i < hs_.length; i++) {
      console.log(hs_a[i].name);
      console.log(hs_a[i].age);
      console.log(hs_a[i].sex);
      console.log(hs_a[i].dtb);
    }
  }
  else if (find === "B") {
    let hs_b = hs.filter((hs) => {
        if(hs.dtb >= 2.5 && hs.dtb <3){
          return hs;
        }
    })
    for (let i = 0; i < hs_b.length; i++) {
      console.log(hs_b[i].name);
      console.log(hs_b[i].age);
      console.log(hs_b[i].sex);
      console.log(hs_b[i].dtb);
    }
  }
  else if (find === "C") {
    let hs_c = hs.filter((hs) => {
        if(hs.dtb >= 3){
          return hs;
        }
    })
    for (let i = 0; i < hs_c.length; i++) {
      console.log(hs_c[i].name);
      console.log(hs_c[i].age);
      console.log(hs_c[i].sex);
      console.log(hs_c[i].dtb);
    }
  }
  else {
    console.log("Nhap sai");
  }
}
